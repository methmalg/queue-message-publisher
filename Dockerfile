FROM openjdk:8-jre-slim
ADD target/pubsub-message-publisher-0.1.0.jar pubsub-message-publisher.jar
VOLUME /tmp
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=local","pubsub-message-publisher.jar"]
